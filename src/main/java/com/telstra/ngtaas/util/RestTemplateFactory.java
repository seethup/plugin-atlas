package com.telstra.ngtaas.util;

import org.springframework.web.client.RestTemplate;

public class RestTemplateFactory {
	
	public static RestTemplate createRestTemplate() {
		
		return new RestTemplate();
		
	}

}
