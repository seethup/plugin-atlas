package com.telstra.ngtaas.errorhandler;

import org.springframework.http.HttpStatus;


public class RESTAPIException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HttpStatus statusCode;
	private String error;
	
	public RESTAPIException(HttpStatus code,String error) {
		super(error);
		this.statusCode=code;
		this.error=error;
	}

	public HttpStatus getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	
}
