package com.telstra.ngtaas.errorhandler;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

public class TaasErrorHandler implements ResponseErrorHandler{

	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void handleError(ClientHttpResponse httpResponse) throws IOException {
		// TODO Auto-generated method stub
		
		if (httpResponse.getStatusCode()
		          .series() == HttpStatus.Series.SERVER_ERROR) {
		            throw new RESTAPIException(httpResponse.getStatusCode(),httpResponse.getStatusText());
		        } else if (httpResponse.getStatusCode()
		          .series() == HttpStatus.Series.CLIENT_ERROR) {
		        	throw new RESTAPIException(httpResponse.getStatusCode(),httpResponse.getStatusText());
		            
		        }
		
	}

}
