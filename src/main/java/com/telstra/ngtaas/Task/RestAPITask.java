package com.telstra.ngtaas.Task;


import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;


public class RestAPITask implements TaskType{

	@Override
	public TaskResult execute(TaskContext taskContext) throws TaskException {
		// TODO Auto-generated method stub
		final BuildLogger buildLogger = taskContext.getBuildLogger();
		
         String suite = taskContext.getConfigurationMap().get("suite");
        
         String versionId = taskContext.getConfigurationMap().get("versionId");
        
         String suiteId = taskContext.getConfigurationMap().get("suiteId");
        
         String projectKey = taskContext.getConfigurationMap().get("projectKey");
         
         String url="http://localhost:2500/postIt";
        
         APICall  apiCall = new APICall(suiteId, projectKey, suite, versionId);
        
         TaskResultBuilder builder=apiCall.makeApiCall(taskContext, url,buildLogger);
       
         final TaskResult taskResult=builder.build();
         
          
         
         return taskResult;
         
         
	}


}
