package com.telstra.ngtaas.Task;

import java.util.Map;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;

public class RestAPITaskConfigurator extends AbstractTaskConfigurator{

	@Override
	public Map<String, String> generateTaskConfigMap(ActionParametersMap params,
			TaskDefinition previousTaskDefinition) {
		// TODO Auto-generated method stub
		final Map<String,String>config=super.generateTaskConfigMap(params, previousTaskDefinition);
		
		config.put("suite", params.getString("suite"));
		config.put("versionId", params.getString("versionId"));
		config.put("suiteId", params.getString("suiteId"));
		config.put("projectKey", params.getString("projectKey"));
		
		return config;
	}

	@Override
	public void populateContextForEdit(Map<String, Object> context, TaskDefinition taskDefinition) {
		// TODO Auto-generated method stub
		super.populateContextForEdit(context, taskDefinition);
		context.put("suite", taskDefinition.getConfiguration().get("suite"));
		context.put("versionId", taskDefinition.getConfiguration().get("versionId"));
		context.put("suiteId", taskDefinition.getConfiguration().get("suiteId"));
		context.put("projectKey", taskDefinition.getConfiguration().get("projectKey"));
	}

	@Override
	public void populateContextForCreate(Map<String, Object> context) {
		// TODO Auto-generated method stub
		super.populateContextForCreate(context);
		context.put("suite", "v1/Network connectivity32");
		context.put("versionId", "10301");
		context.put("suiteId", "59");
		context.put("projectKey", "TRT");
	}
	
	

}
