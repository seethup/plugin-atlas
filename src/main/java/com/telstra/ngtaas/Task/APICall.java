package com.telstra.ngtaas.Task;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.UnknownHttpStatusCodeException;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.TaskContext;

import com.atlassian.bamboo.task.TaskResultBuilder;

import com.telstra.ngtaas.util.RestTemplateFactory;

public class APICall {

	private String suiteId;
	private String projectKey;
	private String suite;
	private String versionId;
	
	public APICall (String suiteId,String projectKey,String suite,String versionId) {
		this.suite=suite;
		this.suiteId=suiteId;
		this.projectKey=projectKey;
		this.versionId=versionId;
	}
	
	public TaskResultBuilder makeApiCall(TaskContext taskContext,String url,BuildLogger buildLogger) {
		             
       final TaskResultBuilder builder= TaskResultBuilder.newBuilder(taskContext).failed(); // Initially assigned failed
       
       RestTemplate template=RestTemplateFactory.createRestTemplate();
       //template.setErrorHandler(new TaasErrorHandler());
       
       buildLogger.addBuildLogEntry("Suite :"+suite+","+"Version ID: "+versionId+" ,SuiteID "+suiteId+", Project Key"+projectKey);
       
       HttpHeaders headers= new HttpHeaders();
       
       headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
       
       MultiValueMap<String,String>map= new LinkedMultiValueMap<String, String>();
       
       map.add("suite", suite);
       map.add("versionId", versionId);
       map.add("suiteId", suiteId);
       map.add("projectKey", projectKey);

       ResponseEntity<String>response=null;
       
       HttpEntity<MultiValueMap<String,String>>request=new HttpEntity<MultiValueMap<String,String>>(map,headers);
       String result="";
       try 
       {
	       	response=template.postForEntity(url, request, String.class);
	       	buildLogger.addBuildLogEntry("Response : "+response);
	       	HttpStatus status=response.getStatusCode();
	        buildLogger.addBuildLogEntry("REST API Status : "+status);
	        if(status==HttpStatus.OK) {
	        	 result = response.getBody();
	        	 buildLogger.addBuildLogEntry("REST API body : "+result);
	        	 builder.success();
	        }            
       }
       catch(ResourceAccessException e)
       {
			buildLogger.addBuildLogEntry(e.getMessage());
       }
       catch (HttpClientErrorException e) {
			buildLogger.addBuildLogEntry(e.getMessage());
			buildLogger.addBuildLogEntry(e.getResponseBodyAsString());
		}
       catch(HttpServerErrorException e) {
			buildLogger.addBuildLogEntry(e.getMessage());
			buildLogger.addBuildLogEntry(e.getResponseBodyAsString());
       }
       catch(UnknownHttpStatusCodeException e) {
       		buildLogger.addBuildLogEntry(e.getMessage());
			buildLogger.addBuildLogEntry(e.getResponseBodyAsString());
       }
		return builder;
	}
}
