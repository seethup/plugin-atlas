package com.telstra.ngtaas.api;

public interface MyPluginComponent
{
    String getName();
}