package ut.com.telstra.ngtaas;

import org.junit.Test;
import com.telstra.ngtaas.api.MyPluginComponent;
import com.telstra.ngtaas.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}